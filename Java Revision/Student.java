import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Student {
	private String studentID;
	private String firstName;
    private String middleName;
    private String lastName;
    private SimpleDateFormat inDateOfBirth = new SimpleDateFormat("dd/mm/yyyy");
    private SimpleDateFormat outDateOfBirth = new SimpleDateFormat("yyyy/dd/mm");
    String outDate;
	public Student(String sid, String fn, String mn, String ln, String dob) throws ParseException {
		studentID = sid;
		firstName = fn;
		middleName = mn;
		lastName = ln;
		Date date = inDateOfBirth.parse(dob);
        outDate = outDateOfBirth.format(date); 
		
	}
	public void setStudentID(String value) {
		studentID = value;
	}
	public void setFirstName(String value) {
		firstName = value;
	}
	public void setMiddleName(String value) {
		middleName = value;
	}
	public void setLastName(String value) {
		lastName = value;
	}
	public void setDOB(String value){
		outDate = value;
	}
	public String getFullName(){
		return firstName+" "+middleName+" "+lastName;
	}
	public void printStudent()
    {
        System.out.println("Name: "+getFullName()+"\nStudent ID: " + studentID + "\nDate of Birth (YYYY/DD/MM): " + outDate);
        System.out.println();      
    }
	
}

