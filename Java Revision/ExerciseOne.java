/*
 * Author: Valeria Alvarez Garduno
 * ID: 2114986
 * Java Assignment Exercise: 1
 * Solve the 4 tasks:
 * 1. Write a Java Main program to create the concatenation of two strings
 * 2. Write a user defined method which take 3 integer parameter and return highest number
 * 3. Write a user defined method using Java loop to print Fibonacci series till N number
 * 4. Write 2 overloaded method (similar to task 2), one find greatest number among 3 numbers and other find greatest number among 4 numbers
 */
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class ExerciseOne {
	static Scanner in = new Scanner(System.in); // 
	public static void main(String[] args) {
		// FIRST TASK
		// ----------------------------------------------------------------------------------------------
		System.out.println("TASK 1: String Concatenation");
		System.out.println("Please enter a word: ");
		String str1 = in.nextLine();
		System.out.println("Please enter a word: ");
		String str2 = in.nextLine();
		String bothStr = str1 + str2;
		System.out.println("Word concatanated: "+bothStr);
		// ----------------------------------------------------------------------------------------------
		// SECOND AND FOURTH TASK
		System.out.println("TASK 2 & 4: Greatest Number (Overloading methods)");
		System.out.println("Enter a number: ");
		int num1 = in.nextInt();
		System.out.println("Enter a number: ");
		int num2 = in.nextInt();
		System.out.println("Enter a number: ");
		int num3 = in.nextInt();
		System.out.println("Do you want to enter another number? (y/n): ");
		String anotherNum = in.next();
		if (anotherNum.equals("y")) {
			System.out.println("Enter a number: ");
			int num4 = in.nextInt();
			int res = getGreatestNum(num1,num2,num3,num4);
			System.out.println("The greatest number is: "+res);
		}
		else {
			int res = getGreatestNum(num1,num2,num3);
			System.out.println("The greatest number is: "+res);
		}
		// ----------------------------------------------------------------------------------------------
		// THIRD TASK
		System.out.println("TASK 3: Fibonacci Serie (Loop)");
		System.out.println("Enter the number of numbers you want in the Fibonacci serie: ");
		int limit = in.nextInt();
		printFibonacciNum(limit);
		// ----------------------------------------------------------------------------------------------
	}
	// SECOND TASK METHOD
	public static int getGreatestNum(int a, int b, int c) {
		List<Integer> nums= Arrays.asList(a,b,c); 
		int res = Collections.max(nums);
		return res;
	}
	// FOURTH TASK METHOD
	public static int getGreatestNum(int a, int b, int c, int d) {
		List<Integer> nums= Arrays.asList(a,b,c,d); 
		int res = Collections.max(nums);
		return res;
	}
	// THIRD TASK METHOD
	public static void printFibonacciNum(int n) {
		int sum = 1;
		int bef1 = 0;
		int bef2 = 1;
		for (int i = 0; i<n; i++) {
			sum = bef1+ bef2;
			bef1 = bef2;
			bef2 = sum;
			System.out.println(sum);
		}
	}
}
