/*
 * Author: Valeria Alvarez Garduno
 * ID: 2114986
 * Java Assignment Exercise 2. Task 3.
 * 
 */
import java.text.ParseException;
import java.util.Random;
import java.util.Scanner;

public class ExerciseTwoTaskThree {
	public static Scanner in = new Scanner(System.in);
	public static void main(String[] args) throws ParseException {
		Student s1 = new Student(getStudentID(), "Valeria", "Alvarez","Garduno","01/06/1998");
		Student s2 = new Student(getStudentID(), "Magdalena", "Vilchis","Lopez","19/07/1998");
		Student s3 = new Student(getStudentID(), "Juan", "Reyes","Mendoza","05/05/1998");
		
		s1.printStudent();
		s2.printStudent();
		s3.printStudent();
		
	}
	public static String getStudentID() {
	    Random ran = new Random();
	    int num = ran.nextInt(999999);
	    return String.format("%06d", num);
	}

}
