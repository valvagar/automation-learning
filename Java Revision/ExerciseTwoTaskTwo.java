/*
 * Author: Valeria Alvarez Garduno
 * ID: 2114986
 * Java Assignment Exercise 2. Task 2.
 * 		Write a program to show exception handling if:
 * 		- Divide a number by zero
 * 		- Assigning a string to null and try to concatenate with another string
 */

public class ExerciseTwoTaskTwo {
	public static void main(String[] args) {
		System.out.println("TASK 2: Exception Handling");
		System.out.println("	a) The average of the sum of three numbers\n");
		System.out.println("Let's sum 5, 8 and 9 and divide them by count = 0\n");
		int a = 5;
		int b = 8;
		int c = 9;
		int count = 0;
		try {
			double avg = a + b + c / count;
			System.out.println(avg);
		}
		catch (ArithmeticException e) {
			System.out.println("Can't divide by zero\n");
			System.out.println("Changing count from 0 to 3...\n");
			count = 3;
			double avg = a + b + c / count;
			System.out.println(avg);
		}
		System.out.println("\n	b) Concatanation of null with string\n");
		System.out.println("str1 = hot\nstr2 = null");
		String str1 = "hot";
		String str2 = null;
		try {
			String word = str1.concat(str2);
			System.out.println(word);
		}
		catch(NullPointerException e) {
			System.out.println("\nYou tried to concatanate a string with a null expression\n");
			System.out.println("Changing str2 = null to str2 = dog...\n");
			str2 = "dog";
			String word = str1.concat(str2);
			System.out.println(word);
		}

	}

}
