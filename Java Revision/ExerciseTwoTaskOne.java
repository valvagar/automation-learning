import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/*
 * Author: Valeria Alvarez Garduno
 * ID: 2114986
 * Java Assignment Exercise 2. Task 1.
 * 		Write a program using recursive methods to generate Fibonacci series.
 */
public class ExerciseTwoTaskOne {
	static Scanner in = new Scanner(System.in);
	public static void main(String[] args) {
		System.out.println("TASK 1: Fibonacci series with recursive method");
		System.out.println("Enter the number of numbers you want in the Fibonacci series: ");
		int n = in.nextInt();
		List<Integer> serie= new ArrayList<Integer>();
		System.out.println();
		for(int i = 1; i<=n;i++) {
			serie.add(fib(i));
		}
		System.out.println("Here's the list: ");
		System.out.println(Arrays.toString(serie.toArray()));
	}
	public static int fib(int num) {
		if (num<=1) {
			return num;
		}
		else {
			return fib(num-1) + fib(num-2);
		}
	}

}
