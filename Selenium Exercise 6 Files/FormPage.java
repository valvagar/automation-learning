import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class FormPage {
	public void submitForm(WebDriver driver, String fName,String lName,String job,String lEdu,String s,String yExp) throws InterruptedException, IOException {

		// FIND LOCATORS
		WebElement firstName = driver.findElement(By.id("first-name")); // FIRST NAME INPUT BOX
		WebElement lastName = driver.findElement(By.id("last-name")); // LAST NAME INPUT BOX
		WebElement jobTitle = driver.findElement(By.id("job-title")); // JOB TITLE INPUT BOX
		WebElement levelEducation = driver.findElement(By.id("radio-button-"+lEdu)); // RADIO BUTTON 2 FROM COLLEGE LEVEL OF EDUCATION
		WebElement sex = driver.findElement(By.id("checkbox-"+s)); // CHECK BOX 2 FOR FEMALE SEX
		WebElement yearsExp = driver.findElement(By.xpath("//select/option["+yExp+"]")); // OPTION OF 2 TO 4 YEARS OF EXPERIENCE IN SELECT ELEMENT
		WebElement date = driver.findElement(By.id("datepicker")); // DATE ELEMENT
		WebElement submit = driver.findElement(By.xpath("//a[@role='button']")); // SUBMIT BUTTON

		// FILL FORM. TAKE SCREENSHOT AFTER FILLING SEX, YEARS OF EXPERIENCE AND DATE FIELDS
		firstName.sendKeys(fName); // FILL WITH FIRST NAME
		lastName.sendKeys(lName); // FILL WITH LAST NAME
		jobTitle.sendKeys(job); // FILL WITH JOB TITLE
		levelEducation.click(); // CLICK RADIO BUTTON
		sex.click(); // CLICK CHECKBOX
		getScreenshot(driver,"C:\\Users\\valva\\eclipse-workspace\\LearningSelenium\\ssSexField.png");
		yearsExp.click(); // CLICK OPTION IN SELECT ELEMENT
		getScreenshot(driver,"C:\\Users\\valva\\eclipse-workspace\\LearningSelenium\\ssYOEField.png");
		date.click(); // CLICK DATE ELEMENT
		WebElement today = driver.findElement(By.xpath("//tr/td[@class='today day']")); // TODAY'S DATE IN DATE ELEMENT
		today.click(); // CLICK TODAY'S DATE
		getScreenshot(driver,"C:\\Users\\valva\\eclipse-workspace\\LearningSelenium\\ssDateField.png");
		submit.click(); // CLICK SUBMIT BUTTON
	}

	// METHOD TO CONFIRM THE PAGE TITLE
	public String getConfirm(WebDriver driver) {
		WebElement confirmTitle = driver.findElement(By.tagName("h1"));
		String title = confirmTitle.getText();
		return title;
	}
	
	// METHOD TO TAKE SCREENSHOT
	public void getScreenshot(WebDriver driver,String path) throws IOException {
		File src = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE); // TAKES SCREENSHOT
		FileUtils.copyFile(src,new File(path)); // SAVES SCREENSHOT IN SPECIFIED PATH
	}
}
