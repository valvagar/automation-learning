import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import junit.framework.Assert;

public class SeleniumExSix {

	WebDriver driver;
	
	@Before // SETTING PAGE
	public void setting() {
		System.setProperty("webdriver.chrome.driver", "C:/Users/valva/chromedriver.exe"); // SET THE KEY FROM THE CHROME DRIVER
		driver = new ChromeDriver(); // SET THE DRIVER AS A CHROMEDRIVER
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5)); // IMPLICIT WAIT TO AVOID LOADING ERRORS
	}
	
	@SuppressWarnings("deprecation")
	@Test
	public void testFillForm() throws InterruptedException, IOException {
		driver.get("https://formy-project.herokuapp.com/form"); // GET TO THE URL
		
		// FILL THE FORM AT THE URL
		// INFORMATION TO FILL
		ReadExcel dataExcel = new ReadExcel(); // Defining a ReadExcel Object
		ArrayList<String> data = dataExcel.getData(); // Get the Array List
		// and define to the corresponding String object
		String fName = data.get(0);
		String lName = data.get(1);
		String job = data.get(2);
		String lEdu = data.get(3);
		String s = data.get(4);
		String yExp = data.get(5);
		
		// POM
		FormPage formPage = new FormPage();
		formPage.submitForm(driver,fName,lName,job,lEdu,s,yExp);
		
		// WAIT FOR SUBMISSION CONFIRMATION ("The form was successfully submitted") AND TAKES SCREENSHOT
		WebDriverWait w = new WebDriverWait(driver,5);
		w.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".alert")));
		formPage.getScreenshot(driver,"C:\\Users\\valva\\eclipse-workspace\\LearningSelenium\\ssSubmitting.png");
		
		// ASSERTION
		String confirmLegend = formPage.getConfirm(driver);
		Assert.assertEquals(confirmLegend,"Thanks for submitting your form");
		
	}

	@After // QUIT BROWSER
	public void out(){
		driver.quit();
	}
}
