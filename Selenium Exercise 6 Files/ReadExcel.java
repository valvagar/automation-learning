import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;

public class ReadExcel {
	String filePath = "C:\\Users\\valva\\eclipse-workspace\\LearningSelenium\\FormData.xls"; // File Path
	String fName; // First Name 
	String lName; // Last Name
	String job; // Job Title
	String lEdu; // Education Level 
	String sex; // Sex
	String yExp; // Years of Experience
	
	// GET DATA METHOD IN AN ARRAY LIST OF STRINGS
	public ArrayList<String> getData() throws IOException {
		File file = new File(filePath); // Create file
		FileInputStream fis = new FileInputStream(file);
		HSSFWorkbook wb = new HSSFWorkbook(fis); // Declare Workbook
		HSSFSheet sheet = wb.getSheet("1"); // Declare name of book's sheet
		
		Iterator<Row> row = sheet.rowIterator(); // Define row Iterator
		row.next(); // Entering row's iteration
		ArrayList<String> data = new ArrayList<String>(); // Define Array List of the data
		HSSFRow eachRow = sheet.getRow(1); // Second Row (zero index)

		// Get data from the specific cells according to the sheet information (zero index)
		// and adding to the Array List
		fName = eachRow.getCell(0).getStringCellValue(); 
		data.add(fName);
		lName = eachRow.getCell(1).getStringCellValue();
		data.add(lName);
		job = eachRow.getCell(2).getStringCellValue();
		data.add(job);
		lEdu = eachRow.getCell(3).getStringCellValue();
		data.add(lEdu);
		sex = eachRow.getCell(4).getStringCellValue();
		data.add(sex);
		yExp = eachRow.getCell(5).getStringCellValue();
		data.add(yExp);
		
		return data; // Return Array List 
	}
}

