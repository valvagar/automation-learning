# Automation Learning

Project for the Automation Learning Workshop at TCS

## Projects/Assignments
1. [x] GitLab Setup and Usage
1. [x] Scrum Overview and Jira Setup
1. [x] Java Revision
1. [x] JUnit & Assertion
1. [x] log4j
1. [x] HTML
1. [x] XPath
1. [x] Selenium
1. [x] Automation Framework
1. [x] Software Testing Overview
1. [ ] Exercise Demo
1. [ ] Interview Practice
