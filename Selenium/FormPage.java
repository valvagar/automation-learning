import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class FormPage {
	public void submitForm(WebDriver driver, String fName,String lName,String job) throws InterruptedException {

		// FIND LOCATORS
		WebElement firstName = driver.findElement(By.id("first-name")); // FIRST NAME INPUT BOX
		WebElement lastName = driver.findElement(By.id("last-name")); // LAST NAME INPUT BOX
		WebElement jobTitle = driver.findElement(By.id("job-title")); // JOB TITLE INPUT BOX
		WebElement levelEducation = driver.findElement(By.id("radio-button-2")); // RADIO BUTTON 2 FROM COLLEGE LEVEL OF EDUCATION
		WebElement sex = driver.findElement(By.id("checkbox-2")); // CHECK BOX 2 FOR FEMALE SEX
		WebElement yearsExp24 = driver.findElement(By.xpath("//select/option[3]")); // OPTION OF 2 TO 4 YEARS OF EXPERIENCE IN SELECT ELEMENT
		WebElement date = driver.findElement(By.id("datepicker")); // DATE ELEMENT
		WebElement submit = driver.findElement(By.xpath("//a[@role='button']")); // SUBMIT BUTTON

		// FILL FORM
		firstName.sendKeys(fName); // FILL WITH FIRST NAME
		lastName.sendKeys(lName); // FILL WITH LAST NAME
		jobTitle.sendKeys(job); // FILL WITH JOB TITLE
		levelEducation.click(); // CLICK RADIO BUTTON
		sex.click(); // CLICK CHECKBOX
		yearsExp24.click(); // CLICK OPTION IN SELECT ELEMENT
		date.click(); // CLICK DATE ELEMENT
		WebElement today = driver.findElement(By.xpath("//tr/td[@class='today day']")); // TODAY'S DATE IN DATE ELEMENT
		today.click(); // CLICK TODAY'S DATE
		submit.click(); // CLICK SUBMIT BUTTON
	}
	
	public String getConfirm(WebDriver driver) {
		WebElement confirmTitle = driver.findElement(By.tagName("h1"));
		String title = confirmTitle.getText();
		return title;
	}
}
