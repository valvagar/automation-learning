import static org.junit.Assert.*;

import java.time.Duration;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import junit.framework.Assert;

public class SeleniumExFour {

	WebDriver driver;
	
	@Before // SETTING PAGE
	public void setting() {
		System.setProperty("webdriver.chrome.driver", "C:/Users/valva/chromedriver.exe"); // SET THE KEY FROM THE CHROME DRIVER
		driver = new ChromeDriver(); // SET THE DRIVER AS A CHROMEDRIVER
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5)); // IMPLICIT WAIT TO AVOID LOADING ERRORS
	}
	
	@SuppressWarnings("deprecation")
	@Test
	public void testFillForm() throws InterruptedException {
		driver.get("https://formy-project.herokuapp.com/form"); // GET TO THE URL
		
		// FILL THE FORM AT THE URL
		// INFORMATION TO FILL
		String fName = "Valeria";
		String lName = "Alvarez";
		String job = "IT Trainee";
		
		// POM
		FormPage formPage = new FormPage();
		formPage.submitForm(driver,fName,lName,job);
		
		// WAIT FOR SUBMISSION CONFIRMATION ("The form was successfully submitted")
		WebDriverWait w = new WebDriverWait(driver,5);
		w.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".alert")));
		
		// ASSERTION
		String confirmLegend = formPage.getConfirm(driver);
		Assert.assertEquals(confirmLegend,"Thanks for submitting your form");
		
	}

	@After // QUIT BROWSER
	public void out() {
		driver.quit();
	}
}
