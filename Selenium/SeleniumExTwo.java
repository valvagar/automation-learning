import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class SeleniumExTwo {
	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "C:/Users/valva/chromedriver.exe"); // SET THE KEY FROM THE CHROME DRIVER
		WebDriver driver = new ChromeDriver(); // SET THE DRIVER AS A CHROMEDRIVER
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5)); // IMPLICIT WAIT TO AVOID LOADING ERRORS
		driver.get("https://formy-project.herokuapp.com/form"); // GET TO THE URL
		
		// FILL THE FORM AT THE URL
		// INFORMATION TO FILL
		String fName = "Valeria";
		String lName = "Alvarez";
		String job = "IT Trainee";
		
		// LOCATORS
		WebElement firstName = driver.findElement(By.id("first-name")); // FIRST NAME INPUT BOX
		WebElement lastName = driver.findElement(By.id("last-name")); // LAST NAME INPUT BOX
		WebElement jobTitle = driver.findElement(By.id("job-title")); // JOB TITLE INPUT BOX
		WebElement levelEducation = driver.findElement(By.id("radio-button-2")); // RADIO BUTTON 2 FROM COLLEGE LEVEL OF EDUCATION
		WebElement sex = driver.findElement(By.id("checkbox-2")); // CHECK BOX 2 FOR FEMALE SEX
		WebElement yearsExp24 = driver.findElement(By.xpath("//select/option[3]")); // OPTION OF 2 TO 4 YEARS OF EXPERIENCE IN SELECT ELEMENT
		WebElement date = driver.findElement(By.id("datepicker")); // DATE ELEMENT
		WebElement submit = driver.findElement(By.xpath("//a[@role='button']")); // SUBMIT BUTTON
		
		// FILLING AND CLICKING
		firstName.sendKeys(fName); // FILL WITH FIRST NAME
		lastName.sendKeys(lName); // FILL WITH LAST NAME
		jobTitle.sendKeys(job); // FILL WITH JOB TITLE
		levelEducation.click(); // CLICK RADIO BUTTON
		sex.click(); // CLICK CHECKBOX
		yearsExp24.click(); // CLICK OPTION IN SELECT ELEMENT
		date.click(); // CLICK DATE ELEMENT
		WebElement today = driver.findElement(By.xpath("//tr/td[@class='today day']")); // TODAY'S DATE IN DATE ELEMENT
		today.click(); // CLICK TODAY'S DATE
		Thread.sleep(1000); // WAIT 1 SEC
		submit.click(); // CLICK SUBMIT BUTTON
		Thread.sleep(1000); // WAIT 1 SEC
		driver.quit(); // QUIT BROWSER
	}
}
